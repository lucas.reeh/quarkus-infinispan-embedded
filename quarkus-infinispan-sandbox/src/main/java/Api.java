import org.infinispan.Cache;
import org.infinispan.manager.EmbeddedCacheManager;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.nio.charset.StandardCharsets;
import java.util.logging.Logger;

@Path("/api")
public class Api {
  private final static Logger log = Logger.getLogger(Api.class.getName());

  @Inject
  EmbeddedCacheManager emc;

  @Path("{cacheName}/{id}")
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  public String get(@PathParam("cacheName") String cacheName, @PathParam("id") String id) {
    log.info("Retrieving " + id + " from " + cacheName);
    Cache<String, String> cache = emc.getCache(cacheName);
    return cache.get(id);
  }

  @Transactional
  @Path("{cacheName}/{id}/{value}")
  @PUT
  @Produces(MediaType.TEXT_PLAIN)
  public String put(@PathParam("cacheName") String cacheName, @PathParam("id") String id, @PathParam("value") String value) {
    log.info("Putting " + id + " with value: " + value + " into " + cacheName);
    emc.getCache(cacheName).put(id, value);
    return (String) emc.getCache(cacheName).get(id);
  }

  @Path("{cacheName}/{id}")
  @DELETE
  @Produces(MediaType.TEXT_PLAIN)
  public void remove(@PathParam("cacheName") String cacheName, @PathParam("id") String id) {
    log.info("Removing " + id + " from " + cacheName);
    emc.getCache(cacheName).remove(id);
  }

  @Path("/info")
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  public String simpleCluster() {
    StringBuilder sb = new StringBuilder("Cluster-Info: \n");
    emc.getMembers().forEach(a -> {
      sb.append(a.toString());
      sb.append("\n");
    });
    return sb.toString();
  }

}
