import io.quarkus.runtime.StartupEvent;
import org.infinispan.Cache;
import org.infinispan.commons.CacheListenerException;
import org.infinispan.configuration.cache.CacheMode;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.global.GlobalConfigurationBuilder;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;

import javax.annotation.Priority;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.util.concurrent.TimeUnit;

@ApplicationScoped
public class Boot {

  @Inject
  EmbeddedCacheManager emc;

  void onStart(@Observes StartupEvent ev) {
    // emc.defineConfiguration("dist", emc.getDefaultCacheConfiguration());
    Cache<String, String> cache = emc.getCache("dist");
    cache.put("key1", "value1");

  }

}
