import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

@QuarkusTest
public class TestSecond {

  @Test
  public void testSecond() {
    RestAssured
        .given()
        .when()
        .put("http://localhost:8080/api/dist/key1/newvalue")
        .then().assertThat().statusCode(HttpStatus.SC_OK);
    String as = RestAssured
        .given()
        .when()
        .get("http://localhost:8081/api/dist/key1")
        .then().assertThat().statusCode(HttpStatus.SC_OK)
        .extract().asString();
    assertEquals("newvalue", as);
  }

}
