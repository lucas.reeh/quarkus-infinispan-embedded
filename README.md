# quarkus-infinispan-embedded

Example using quarkus infinispan embedded extension with jdbc ping config, postgres config.

## Run

Start database

```docker-compose up```

In quarkus-infinispan-sandbox run quarkus in dev with `./mvnw quarkus:dev`.

Test:

Run Tests in Test-Directory for trying out distributed cache.

# Sources

https://github.com/danieloh30/embedded-caches-quarkus

https://github.com/jgroups-extras/quarkus-jgroups-chat
